from django.urls import path
from home import views
from django.conf import settings

app_name = "home_app"

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('empleados/', views.empleados, name="empleados"),
    path('lider/', views.lider, name="lider"),
    path('actividades/', views.Actividades, name="actividades"),
    path('estado/', views.estadoAct, name="estado"),
    path('reportes/', views.reportes, name="reportes"),
    path('lider_new/', views.lider_registro, name="lider_new"),
    path('empleados_new/', views.empleados_registro, name="empleados_new"),
    path('actividad_new/', views.actividades_registro, name="actividad_new"),
    path('estadoAct_new/', views.tareas_registro_view, name="estadoAct_new"),




]