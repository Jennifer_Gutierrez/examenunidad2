from django.contrib import admin

from .models import Empleado, Lider, Actividad, EstadoAct, Puesto

# Register your models here.

admin.site.register(Empleado)
admin.site.register(Lider)
admin.site.register(Actividad)
admin.site.register(EstadoAct)
admin.site.register(Puesto)