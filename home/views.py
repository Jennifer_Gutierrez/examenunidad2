from django.shortcuts import render, redirect
from .forms import AltaDeEmpleado, AltaDeLider, AltaDeActividad, AltaDeEstadoAct
from .models import Empleado, Lider, Actividad, EstadoAct, Puesto
from django.urls import reverse_lazy



# Create your views here.

def inicio(request):
    return render(request, "home/inicio.html", {})

def empleados(request):
    empleados = list(Empleado.objects.all())

    context = {
        'empleados' : empleados
    }
    return render(request, "home/empleados.html", context)

def lider(request):
    lider = list(Lider.objects.all())

    context = {
        'lider' : lider
    }
    return render(request, "home/lider.html", context)

def Actividades(request):
    actividades = list(Actividad.objects.all())

    context = {
        'actividades' : actividades
    }
    return render(request, "home/actividades.html", context)

def estadoAct(request):
    estado = list(EstadoAct.objects.all())

    context = {
        'estado' : estado
    }
    return render(request, 'home/estado.html', context)

### Reportes ###

class DesempenoDeEmpleado:
    def __init__(self, empleado, cantidad_de_actividad):
        self.empleado = empleado
        self.cantidad_de_actividad = cantidad_de_actividad

class ActividadPorEmpleado:
    def __init__(self, empleado, actividad):
        self.empleado = empleado
        self.actividad = actividad

def reportes(request):
    empleado_ex = list(Empleado.objects.all())

    desempeno_de_empleados = []
    actividad_de_empleados = []
    for empleado in empleado_ex:
        cantidad_de_actividades = EstadoAct.objects.filter(empleado__id=empleado.id).count()
        desempeno_de_empleado = DesempenoDeEmpleado(empleado, cantidad_de_actividades)
        desempeno_de_empleados.append(desempeno_de_empleado)

        actividad_de_empleado_queryset = EstadoAct.objects.filter(empleado__id=empleado.id)
        actividad_de_empleado = ActividadPorEmpleado(empleado, list(actividad_de_empleado_queryset))
        actividad_de_empleados.append(actividad_de_empleado)

    context = {
        'desempeno_de_empleados' : desempeno_de_empleados,
        'actividad_de_empleados' : actividad_de_empleados
    }
    return render(request, 'home/reportes.html', context)

###  Registros ###
def lider_registro(request):
    form = AltaDeLider()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeLider(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            apellido_paterno = form.cleaned_data['apellido_paterno']
            apellido_materno = form.cleaned_data['apellido_materno']

            nuevo_lider = Lider(nombre=nombre, apellido_paterno=apellido_paterno, apellido_materno=apellido_materno)
            nuevo_lider.save()
            return redirect("home_app:lider")


    return render(request, 'home/lider_new.html', context)

def empleados_registro(request):
    form = AltaDeEmpleado()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeEmpleado(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            apellido_paterno = form.cleaned_data['apellido_paterno']
            apellido_materno = form.cleaned_data['apellido_materno']
            puesto = form.cleaned_data['puesto']

            nuevo_empleado = Empleado(nombre=nombre, apellido_paterno=apellido_paterno, apellido_materno=apellido_materno, puesto=puesto)
            nuevo_empleado.save()
            return redirect("home_app:empleados")

    return render(request, 'home/empleados_new.html', context)

def actividades_registro(request):
    form = AltaDeActividad()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeActividad(request.POST)
        if form.is_valid():
            nombre = form.cleaned_data['nombre']
            descripcion = form.cleaned_data['descripcion']

            nueva_actividad = Actividad(nombre=nombre, descripcion=descripcion)
            nueva_actividad.save()
            return redirect("home_app:actividades")

    return render(request, "home/actividad_new.html", context)

def tareas_registro_view(request):
    form = AltaDeEstadoAct()

    context = {
        'form' : form
    }

    if request.method == "POST":
        form = AltaDeEstadoAct(request.POST)
        if form.is_valid():
            actividad = form.cleaned_data['actividad']
            lider = form.cleaned_data['lider']
            empleado = form.cleaned_data['empleado']
            fecha_de_inicio = form.cleaned_data['fecha_de_inicio']
            fecha_de_finalizacion = form.cleaned_data['fecha_de_finalizacion']

            nueva_act = EstadoAct(actividad=actividad, lider=lider, empleado=empleado , fecha_de_inicio=fecha_de_inicio, fecha_de_finalizacion=fecha_de_finalizacion)
            nueva_act.save()
            return redirect("home_app:estado")
    
    return render(request, 'home/estadoAct_new.html', context)


